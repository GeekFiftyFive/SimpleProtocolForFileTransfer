#include "spfft-server.h"
#include <stdlib.h>
#include <nanomsg/nn.h>
#include <nanomsg/reqrep.h>
#include <time.h>
#include <dirent.h>
#include "spfft-shared.h"
#include <uthash.h>

#define BUFFER_SIZE 65536

typedef struct filePaths{
    __uint32_t bufferSize, length;
    char **paths;
} filePaths;

//Stores the index at which a file path is stored
//This is used to lookup file paths when a session is started
typedef struct fileIndex{
    char *path;
    __uint32_t index;
    UT_hash_handle hh;
} fileIndex;

typedef struct session{
    FILE *fp;
    int secret;
    float progress;
    __uint32_t block;
    __uint32_t index;
} session;

struct spffts_iface {
    int sock;
    long delay;
    session *sessions;
    __uint32_t sessionCount;
    __uint32_t lastIndex;
    __uint32_t size;
    filePaths files;
    fileIndex *indices;
};

spffts_iface spffts_openInterface(char *url, long delay){
       //Create interface
    spffts_iface iface = malloc(sizeof(*iface));
    iface -> sock = nn_socket(AF_SP, NN_REP);
    iface -> delay = delay;
    iface -> size = 1024;
    iface -> sessions = malloc(sizeof(struct session) * iface -> size);
    iface -> sessionCount = 0;
    iface -> files.bufferSize = 4;
    iface -> files.length = 0;
    iface -> files.paths = malloc(iface -> files.bufferSize * sizeof(void*));
    iface -> indices = NULL;
    
    //Seed the random number generator for shared secrets
    time_t t;
    srand((unsigned) time(&t));

    //Initialise the file pointers for each session to NULL
    for(int i = 0; i < iface -> size; i++) iface -> sessions[i].fp = NULL;
    iface -> lastIndex = 0;
    if(iface -> sock < 0 || nn_bind(iface -> sock, url) < 0)
        fprintf(stderr, "Error binding socket with IP %s\n", url);

    return iface;
}

void spffts_closeInterface(spffts_iface iface){
    free(iface -> sessions);
    for(int i = 0; i < iface -> files.length; i++) free(iface -> files.paths[i]);
    free(iface -> files.paths);
    nn_shutdown(iface -> sock, 0);
    free(iface);
}

char *validatePath(char *path){
    char *out = NULL;

    // Check for substring "./" as this could be used to get into
    // folders that shouldn't be accessed
    char *occur = strstr(path, "./");
    if(occur) {
        return out;
    }
    
    // Strings starting with '/' start with './' instead
    if(path[0] == '/'){
        out = malloc(strlen(path) + 2);
        sprintf(out, ".%s", path);
    } else {
        out = malloc(strlen(path) + 1);
        strcpy(out, path);
    }

    return out;
}

void getList(spffts_iface iface, char *message){
    DIR *dir;
    struct dirent *ent;
    char *path = validatePath(message + 1);
    if(!path){
        nn_send(iface -> sock, "list_err", 9, 0);
        return;
    }
    dir = opendir(path);
    int size = 10, offset = 0;
    char *list = malloc(size);
    if(dir){
        while((ent = readdir(dir))){
            if(offset + strlen(ent -> d_name) + 2 > size){
                size *= 2;
                size += strlen(ent -> d_name) + 2;
                list = realloc(list, size);
            }
            strcat(list, " ");
            strcat(list, ent -> d_name);
            offset += strlen(ent -> d_name) + 1;
        }
        nn_send(iface -> sock, list, strlen(list) + 1, 0);
    } else nn_send(iface -> sock, "list_err", 9, 0);
    free(list);
    free(path);

    closedir(dir);
}

void updateLastIndex(spffts_iface iface){
    while(iface -> sessions[iface -> lastIndex].fp){
        if(iface -> lastIndex > iface -> size){
            iface -> size *= 2;
            iface -> sessions = realloc(iface -> sessions, iface -> size);
        }
        iface -> lastIndex++;
    }
}

void getBlock(spffts_iface iface, char* message){
    spfft_clientSession session;
    memcpy(&session, message + 1, sizeof(spfft_clientSession));
    __uint32_t id = session.id;
    if(id > iface -> size) {
        nn_send(iface -> sock, "inv_size", 9, 0);
        return;
    }
    if(!iface -> sessions[id].fp || iface -> sessions[id].index != session.index){
        //Create a new session
        updateLastIndex(iface);
        id = iface -> lastIndex;
        iface -> sessions[id].fp = fopen(message + 1, "r"); //Get filename from index
        iface -> sessions[id].secret = rand();
        iface -> sessions[id].progress = 0.f;
        iface -> sessions[id].block = session.block;
        iface -> sessions[id].index = session.index;
        if(session.block != iface -> sessions[id].block){
            iface -> sessions[id].block = session.block;
            fseek(iface -> sessions[id].fp, (long int) session.block * BUFFER_SIZE, SEEK_SET);
        }
    }
    if(session.secret != iface -> sessions[id].secret){
        nn_send(iface -> sock, "inv_secret", 11, 0);
        return;
    }

    void *block = malloc(BUFFER_SIZE + sizeof(size_t) + sizeof(id));
    size_t read = fread(block + sizeof(size_t) + sizeof(id), 1, BUFFER_SIZE, iface -> sessions[id].fp);
    memcpy(block, &read, sizeof(size_t));
    memcpy(block + sizeof(size_t), &id, sizeof(id));
    if(read < BUFFER_SIZE) {
        fclose(iface -> sessions[id].fp);
        iface -> sessions[id].fp = NULL; //Make session slot available again
        iface -> lastIndex = id;
        iface -> sessionCount--;
    }
    nn_send(iface -> sock, block, sizeof(size_t) + sizeof(id) + BUFFER_SIZE, 0);
    free(block);
}

void startSession(spffts_iface iface, char *message){
    __uint32_t oldID = iface -> lastIndex;
    updateLastIndex(iface);
    __uint32_t id = iface -> lastIndex;
    char *path = validatePath(message + 1);
    iface -> sessions[id].fp = fopen(path, "r");
    if(!iface -> sessions[id].fp) {
        fprintf(stderr, "Invalid filename: %s\n", message + 1);
        nn_send(iface -> sock, "inv_filename", 13, 0);
        iface -> lastIndex = oldID;
        return;
    }
    iface -> sessions[id].secret = rand();
    iface -> sessions[id].progress = 0.f;
    iface -> sessions[id].block = 0;

    //Check if path is indexed, if not, index it
    fileIndex *index;
    HASH_FIND_STR(iface -> indices, path, index);
    if(!index){ //The key was not found in the hash
        iface -> files.length++;
        if(iface -> files.length == iface -> files.bufferSize){
            iface -> files.bufferSize *= 2;
            iface -> files.paths = realloc(iface -> files.paths, iface -> files.bufferSize * sizeof(void*));
        }

        iface -> files.paths[iface -> files.length] = malloc(strlen(path) + 1);
        strcpy(iface -> files.paths[iface -> files.length], path);
        index = malloc(sizeof(*index));
        index -> index = iface -> files.length;
        index -> path = iface -> files.paths[iface -> files.length];
        HASH_ADD_STR(iface -> indices, path, index);
    }
    iface -> sessions[id].index = index -> index;
    iface -> sessionCount++;
    spfft_clientSession session = {id, iface -> sessions[id].secret, 0, index -> index};
    nn_send(iface -> sock, &session, sizeof(spfft_clientSession), 0);
    free(path);
}

void pollRequests(spffts_iface iface){
    char *message = NULL;
    __uint8_t loop = 1;
    int len;
    while(loop){
        len = nn_recv(iface -> sock, &message, NN_MSG, 0);

        switch(message[0]){
            case '0': //Begin transfer
                printf("Beginning transfer\n");
                startSession(iface, message);
                break;
            case '1': //Get next block
                getBlock(iface, message);
                break;
            case '2': //Get list of files in directory
                getList(iface, message);
                break;
            case '3': //Shutdown TODO: Check parameters
                loop = 0;
                nn_send(iface -> sock, &iface -> sessionCount, sizeof(iface -> sessionCount), 0);
                break;
            default:
                break;
        }
        nn_freemsg(message);
    }
}
