#include <stdio.h>
#include "../src/spfft-server.h"

int main(int argc, char* argv[]){
    if(argc < 2){
        printf("Usage: SPFFT-server <url>\n");
        return 0;
    }
    spffts_iface iface = spffts_openInterface(argv[1], 1000);
    pollRequests(iface);
    spffts_closeInterface(iface);
    return 0;
}