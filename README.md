# Simple Protocol For File Transfer
A simple protocol for transfering files via TCP/IP. WARNING: THIS IS EARLY WIP SOFTWARE.

## Dependencies
 * nanomsg
 * uthash

## Building
 * Create the directories `obj` and `build`
 * In order to build the server and client libraries, run `make`.
 * To build the client and server examples, run `make build/SPFFT-client` and `make build/SPFFT-server` respectively

 ## Running
  * Both the sample server and sample client take a url as there only argument
  * The url should take the form of `tcp://ip:port`
  * It is also possible to run them using `make run-server` and `make run-client`, which will run them with the url `tcp://127.0.0.1:8080`
